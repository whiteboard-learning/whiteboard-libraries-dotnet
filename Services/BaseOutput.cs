using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Http;
using System.Text;
using System.Net;

using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Whiteboard.Services {
    public class BaseOutput {
    	public string RawOutput;
    	public JObject ParsedOutput;

        public bool IsValid = true;

    	// initialise the output
    	public BaseOutput(string output) {
			try {
                ParsedOutput = JObject.Parse(output);
            }

            catch (Exception e) {
                // BaseOutput.IsValid set to false
                IsValid = false;
            }
    	}

		//  __  __ _____ _____  _    
		// |  \/  | ____|_   _|/ \   
		// | |\/| |  _|   | | / _ \  
		// | |  | | |___  | |/ ___ \ 
		// |_|  |_|_____| |_/_/   \_\
		// 

		// Request Information

        // get objects
        public object GetHeaders() => GetPath("headers");
        public object GetResponse() => GetPath("response");
        public object GetResponseMessages() => GetPath("response.messages");
        public object GetResponseBody() => GetPath("response.data");
		
        // get single strings
        public string GetResponseTime() => ConvertToString(GetPath("headers.responseTime", true));
        public string GetStatus() => ConvertToString(GetPath("headers.status", true));
        public string GetService() => ConvertToString(GetPath("headers.service", true));
        public string GetServerID() => ConvertToString(GetPath("headers.serverID", true));

    	// recurse through the paths
    	public object GetPath(string path, bool first = false) {
    		// return null is ParsedOutput is empty
            if (ParsedOutput == null) return null;

            // check the result for a given lookup
    		IEnumerable<Newtonsoft.Json.Linq.JToken> LookupResult = ParsedOutput.SelectTokens("$." + path);

    		if (first == true) {
    			JToken result = LookupResult.First();
                return (result != null) ? result : null;
    		}

    		else {
    			return LookupResult;
    		}
    	}

        // helper function
        public string ConvertToString(object input) {
            if (input == null) return "";
            else {
                if (input.GetType().ToString() == "string") return (string)input;
                else return input.ToString();
            }
        }

        public int? ConvertToInt(object input) {
            if (input == null) return null;
            else return (int?)Convert.ToInt32(input);
        }
    }
}
