using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Http;
using System.Text;
using System.Net;

using Flurl;
using Flurl.Http;

namespace Whiteboard.Services {
    public class BaseService {
    	public static object GetServiceInstance(string name) {
    		// TODO: IMPLEMENT CONSUL SERVICE DISCOVERY
    		// CONSUL: GET SERVICE INSTANCES
    		
    		// RETURN A STATIC HOST FOR NOW
    		return "http://" + name + "service.consul/";
    	}

    	public async static Task<string> SendRequest(string serviceName, string path = "/", object data = null) {
			// get the authentication host
			string instance = (string)GetServiceInstance(serviceName);
			if (instance == null) return null;

			// form the url
			string url = instance + path;

			// create new uri object to check validity of url
			bool valid = Uri.TryCreate(url, UriKind.Absolute, out Uri uri);
			if (valid == null) return null;

			// send a request to it
			try {
				var response = await instance
								.AppendPathSegment(path)
								.PostUrlEncodedAsync(data)
								.ReceiveString();

				return response;

			} catch (FlurlHttpTimeoutException ex) {
				return null;

			} catch (FlurlHttpException ex) {
				return null;

			} catch (Exception e) {
				return null;
			}
    	}

    	public async static Task<string> SendRequestManual(string url, Dictionary<string, object> data = null) {
			// create new uri object to check validity of url
			bool valid = Uri.TryCreate(url, UriKind.Absolute, out Uri uri);
			if (valid == null) return null;

			// send a request to it
			var response = await (uri.Scheme + "://" + uri.Host)
							.AppendPathSegment(uri.AbsolutePath)
							.PostUrlEncodedAsync(data)
							.ReceiveString();

			return response;
    	}
    }
}
