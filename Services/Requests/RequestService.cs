using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Http;
using System.Text;

using Whiteboard.Services;

namespace Whiteboard.Services.Requests {
    public class RequestService : BaseService {
        public static BaseOutput SendRequest(string url, Dictionary<string, object> data = null) {
        	object result = BaseService.SendRequestManual(url, data).Result;
        	return new BaseOutput((string)result);
        }
    }
}
