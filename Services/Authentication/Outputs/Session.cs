using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Http;
using System.Text;

using Whiteboard.Services;

using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Whiteboard.Services.Authentication {
    public class Session : BaseOutput {
    	// initializer
    	public Session(string output) : base(output) {}

    	// methods to get properties
    	// access tokens
		public object GetAccessTokenObject() => GetPath("response.data.accessToken");
		public string GetAccessToken() => (string)GetPath("response.data.accessToken.token", true);
		public string GetAccessTokenExpiry() => (string)GetPath("response.data.accessToken.expiry", true);

		// user information
		public object GetUser() => GetPath("response.data.user");
		public int? GetUserId() => ConvertToInt(GetPath("response.data.user.id", true));
		public string GetUserFirstName() => ConvertToString(("response.data.user.firstName", true));
		public string GetUserLastName() => ConvertToString(("response.data.user.lastName", true));
		public string GetUserName() => ConvertToString(("response.data.user.name", true));
		public string GetUserRole() => ConvertToString(("response.data.user.role", true));

		// user permissions
		public object GetPermissions() => GetPath("response.data.permissions");
    }
}