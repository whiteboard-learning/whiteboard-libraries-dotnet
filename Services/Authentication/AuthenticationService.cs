using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Http;
using System.Text;

using Whiteboard.Services;

namespace Whiteboard.Services.Authentication {
    public class AuthenticationService : BaseService {
        public static Session VerifyAccessToken(string accessToken) {
            object result = BaseService.SendRequest("authentication", "/api/session/check", new Dictionary<string, string> {
                { "accessToken", accessToken }
            }).Result;

            Session session = new Session((string)result);
            return session;
        }
    }
}
