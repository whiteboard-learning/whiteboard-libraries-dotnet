using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Http;
using System.Text;

using System.Configuration;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Rewrite;

using System.Threading.Tasks;
using System.Net;
using System.Web.Http;
using System.Reflection;

using Whiteboard.Services;

namespace Whiteboard.Services.Http {
    class APIResponseService {
        /**
         * Response Headers
         * Includes Start Time, End Time, Response Time, Requested Endpoint
         */
        private Dictionary<string, object> Headers = new Dictionary<string, object>();

        /**
         * Status Code
         */
        public string Status = "UNKNOWN";

        /**
         * Response Data
         */
        public Dictionary<string, object> Data = new Dictionary<string, object>();

        /**
         * Success Messages
         */
        public List<Dictionary<string, object>> SuccessMessages = new List<Dictionary<string, object>>();

        /**
         * Warnings
         */
        public List<Dictionary<string, object>> Warnings = new List<Dictionary<string, object>>();

        /**
         * Errors
         */
        public List<Dictionary<string, object>> Errors = new List<Dictionary<string, object>>();

        /**
         * Initialise the ResponseService object,
         * which is mainly for the headers
         */
        public APIResponseService() {
            Headers = new Dictionary<string, object>() {
                [ "service" ] =  Assembly.GetEntryAssembly().GetName().Name,
                [ "serverID" ] = Dns.GetHostName(),
                [ "responseTime" ] = DateTime.Now
            };
        }

        /**
         * Set status
         * 
         * @param array Status Status Code
         */
        public void SetStatus(string status) {
            Status = status;
        }

        public void SetData(Dictionary<string, object> data) {
            Data = data;
        }

        public void SetSuccessMessages(List<Dictionary<string, object>> successMessages) {
            SuccessMessages = successMessages;
        }

        public void AppendSuccessMessage(string message, string messageContent = null, int? duration = null) {
            // upgraded messages - comes with title and structure
            Dictionary<string, object> Message;

            if (messageContent == null) {
                Message = new Dictionary<string, object>() {
                    [ "title" ] = "Message",
                    [ "message" ] = message,
                    [ "duration" ] = messageContent
                };
            }

            else {
                Message = new Dictionary<string, object>() {
                    [ "title" ] = message,
                    [ "message" ] = messageContent,
                    [ "duration" ] = duration
                };
            }

            SuccessMessages.Add(Message);
        }

        public void SetWarnings(List<Dictionary<string, object>> Warnings) {
            Warnings = Warnings;
        }

        public void AppendWarning(string warning, string warningContent = null, int? duration = null) {
            // upgraded messages - comes with title and structure
            Dictionary<string, object> Warning;

            if (warningContent == null) {
                Warning = new Dictionary<string, object>() {
                    [ "title" ] = "Warning",
                    [ "message" ] = warning,
                    [ "duration" ] = warningContent
                };
            }

            else {
                Warning = new Dictionary<string, object>() {
                    [ "title" ] = warning,
                    [ "message" ] = warningContent,
                    [ "duration" ] = duration
                };
            }

            Warnings.Add(Warning);
        }

        public void SetErrors(List<Dictionary<string, object>> errors) {
            errors = errors;
        }

        public void AppendError(string error, string errorContent = null, int? duration = null) {
            // upgraded messages - comes with title and structure
            Dictionary<string, object> Error;

            if (errorContent == null) {
                Error = new Dictionary<string, object>() {
                    [ "title" ] = "Error",
                    [ "message" ] = errorContent,
                    [ "duration" ] = duration
                };
            }

            else {
                Error = new Dictionary<string, object>() {
                    [ "title" ] = error,
                    [ "message" ] = errorContent,
                    [ "duration" ] = duration
                };
            }

            Errors.Add(Error);
        }

        /**
         * Return the response data in JSON.
         */
        public object Output() {
            Headers["status"] = Status;

            // return the structure as instructed
            return new {
                headers = Headers,
                response = new {
                    messages = new {
                        success = SuccessMessages,
                        warnings = Warnings,
                        errors = Errors,
                    },

                    data = Data
                }
            };
        }
    }
}