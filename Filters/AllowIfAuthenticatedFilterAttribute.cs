using System;
using System.Collections.Generic;

using System.Configuration;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Rewrite;

using System.Threading.Tasks;
using System.Net;
using System.Web.Http;
using System.Reflection;

using Whiteboard.Services.Http;
using Whiteboard.Services.Authentication;

namespace Whiteboard.Filters {
    public class AllowIfAuthenticatedFilterAttribute : ActionFilterAttribute {
        public override void OnActionExecuting(ActionExecutingContext context) {
            // get the access token
            string accessToken = context.HttpContext.Request.Form["accessToken"];

            // get authentication information
            Session session = AuthenticationService.VerifyAccessToken(accessToken);

            if (session.GetStatus() == "VALID") {
                context.HttpContext.Items.Add("Session", session);
            }

            else {
                APIResponseService response = new APIResponseService();
                response.SetStatus("FORBIDDEN");
                context.Result = new JsonResult(response.Output());
            }
        }

        public override void OnActionExecuted(ActionExecutedContext context) {
            base.OnActionExecuted(context);
        }
    }
}